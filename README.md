# S P R E A D S H E E T | challenge

Challenges: change propagation, widget customization, implementing a more authentic/involved GUI application.
[DOWNLOAD APK](https://drive.google.com/file/d/1XcSn8chii-YeF3HxKLDLB0opybISE7Gz/view?usp=sharing)

### SCREENSHOTS
-----------

![init-view](screenshots/spreadsheet_i.jpeg "Main-view application")
![action-cell](screenshots/spreadsheet_ii.jpeg "An action cell in table-layout")

### LANGUAGES, LIBRARIES AND TOOLS USED

* [Kotlin](https://kotlinlang.org/)
* [Android Architecture Components](https://developer.android.com/topic/libraries/architecture/index.html)
* [TableLayout](https://developer.android.com/reference/android/widget/TableLayout)
* [ViewBinding](https://developer.android.com/topic/libraries/view-binding?hl=es-419)

### REQUIREMENTS

* JDK 1.8
* [Android SDK](https://developer.android.com/studio/index.html)
* [Kotlin Version](https://kotlinlang.org/docs/releases.html)
* [MIN SDK 23](https://developer.android.com/preview/api-overview.html))
* [Build Tools Version 30.0.3](https://developer.android.com/studio/releases/build-tools)
* Latest Android SDK Tools and build tools.

### ARCHITECTURE

- N/A Just activity and view.

## L I C E N S E

Under the [MIT license](https://opensource.org/licenses/MIT).

