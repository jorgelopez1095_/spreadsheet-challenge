package com.hugo.challenge.spreadsheet

import android.graphics.Color
import android.os.Bundle
import android.text.InputType
import android.view.Gravity
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TableRow
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.hugo.challenge.spreadsheet.databinding.ActivitySpreadsheetBinding

const val EMPTY_STRING = ""

class SpreadsheetActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySpreadsheetBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySpreadsheetBinding.inflate(layoutInflater)
        setContentView(binding.root)

        loadFeature()
    }

    private fun loadFeature() {
        binding.spreadsheet.bringToFront()

        val alphabetColumns = listOf(
                "init", "A", "B",
                "C", "D", "E",
                "F", "G", "H",
                "I", "J", "K",
                "L", "M", "N",
                "O", "P", "Q",
                "R", "S", "T",
                "U", "V", "W",
                "X", "Y", "Z")

        for (row in 0 .. 99) {
            val itemRow = TableRow(this)
            alphabetColumns.forEach { column ->

                if (row == 0 || column == "init") {
                    val view = TextView(this).apply {
                        setPadding(80, 24, 80, 24)
                        textSize = 18F
                        when {
                            column == "init" && row == 0 -> {
                                text = EMPTY_STRING
                                setBackgroundColor(Color.parseColor("#161616"))
                            }
                            column == "init" && row > 0 -> {
                                text = row.toString()
                                setBackgroundColor(Color.parseColor("#7f8c8d"))
                            }
                            column != "init" && row == 0 -> {
                                text = column
                                setBackgroundColor(Color.parseColor("#7f8c8d"))
                            }
                            else -> {
                                text = "*"
                                setBackgroundColor(Color.parseColor("#ecf0f1"))
                            }
                        }
                    }
                    itemRow.addView(view)
                } else {
                    val view = EditText(this).apply {
                        textSize = 14F
                        setTextColor(Color.parseColor("#161616"))
                        setBackgroundResource(R.drawable.input_cell_style)
                        gravity = Gravity.START
                    }

                    val rowP = TableRow.LayoutParams()
                    rowP.setMargins(10, 0, 0, 0)
                    view.layoutParams = rowP
                    view.inputType = InputType.TYPE_CLASS_NUMBER
                    view.textAlignment = View.TEXT_ALIGNMENT_CENTER
                    view.imeOptions = EditorInfo.IME_ACTION_DONE
                    itemRow.addView(view)
                }
            }
            binding.spreadsheet.addView(itemRow)
        }

    }
}